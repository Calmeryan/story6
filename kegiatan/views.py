from django.shortcuts import render, redirect
from . import forms, models

# Create your views here.
def landing(request):
    if(request.method == "POST"):
        if 'nama_kegiatan' in request.POST:
            input1 = forms.kegiatan(request.POST)
            if(input1.is_valid()):
                input2 = models.Kegiatan()
                input2.nama = input1.cleaned_data['nama_kegiatan']
                input2.save()
        elif 'nama_peserta' in request.POST and 'id_kegiatan' in request.POST:
            input3 = forms.peserta(request.POST)
            if(input3.is_valid()):
                models.Kegiatan.objects.get(id=request.POST['id_kegiatan'])
                input2 = models.peserta()
                input2.nama = input3.cleaned_data['nama_peserta']
                input2.kegiatan=models.Kegiatan.objects.get(id=request.POST['id_kegiatan'])
                input2.save()
    list_kegiatan = models.Kegiatan.objects.all()
    data_kegiatan_lengkap = []
    for kegiatan in list_kegiatan:
        list_peserta = models.peserta.objects.filter(kegiatan = kegiatan)
        data_peserta =[]
        for peserta in list_peserta:
            data_peserta.append(peserta)
        data_kegiatan_lengkap.append((kegiatan,data_peserta))
    return render(request, 'kegiatan.html', {'formkegiatan':forms.kegiatan, 'formpeserta':forms.peserta, 'data_kegiatan':data_kegiatan_lengkap})

    


from django import forms

class kegiatan(forms.Form):
    nama_kegiatan = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Kegiatan',
        'type' : 'text',
        'required' :True
    }))
class peserta(forms.Form):
    nama_peserta = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Nama Peserta',
        'type' : 'text',
        'required' : True
    }))

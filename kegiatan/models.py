from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    nama = models.CharField(blank=False, max_length= 100)
    def __str__(self):
        return self.nama

class peserta(models.Model):
    nama= models.CharField(blank=False,max_length=100)
    kegiatan=models.ForeignKey(Kegiatan,on_delete=models.DO_NOTHING)
    def __str__(self):
        return self.nama